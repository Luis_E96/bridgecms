# BridgeCMS #

BridgeCMS is aiming to be a simple, lightweight and absolutely free Content Management System with similar high standards and functionallity to boards or software you would normally be charged hundreds of dollars for.
Including a completely customizable permission, user, page, style and mod system.

The mod system or mod manager is just a simple tool that will give developers the ability to use functions from The BridgeCMS to create their own small plugins to full scale new software within the CMS.


### Issues/Bugs and Early Version ###

* This is an early version of this Content Management System, there are still bugs, security issues and unimplemented features
* If you see any bugs or if you want to request a feature, use the issue function please
* Feedback is always greately appreciated, so go ahead

### Installation/Setup ###

**The install wizzard is still not finished and the below described steps will be automated**

* Download the full repo
* overwrite the core.cfg.php with the core.template.php but keep the name "core.cfg.php"
* get your domain and add a "/Install" at the end and the current installer will start
* done

### Screenshorts###
![1c0b2e1d71a4d86c862ffdaf865acc4f.png](https://bitbucket.org/repo/oLq9L75/images/4023777850-1c0b2e1d71a4d86c862ffdaf865acc4f.png)
![1ea0260ab5bead39cb98199d366307b2.png](https://bitbucket.org/repo/oLq9L75/images/1043697734-1ea0260ab5bead39cb98199d366307b2.png)
![4a698dce6ce0b7dcf5f7d6bf8562f574.png](https://bitbucket.org/repo/oLq9L75/images/1126825148-4a698dce6ce0b7dcf5f7d6bf8562f574.png)
![5757fe0cbb476e47d1e8fd634541ebee.png](https://bitbucket.org/repo/oLq9L75/images/1532062710-5757fe0cbb476e47d1e8fd634541ebee.png)
![cb61ee4150d4c85620eed1ffc81fbb09.png](https://bitbucket.org/repo/oLq9L75/images/3088194253-cb61ee4150d4c85620eed1ffc81fbb09.png)
![ee5ccb32cef4561fa5aad5e3ac0e72b4.png](https://bitbucket.org/repo/oLq9L75/images/1800794501-ee5ccb32cef4561fa5aad5e3ac0e72b4.png)
![f7bd216840f44c8e6f43c9f3892c3f53.png](https://bitbucket.org/repo/oLq9L75/images/844311389-f7bd216840f44c8e6f43c9f3892c3f53.png)

### Contribut ###

You are absolutely free to contribute, just fork the project or write me directly at support@bridgetroll.de

### Who do I talk to? ###

* This Project is in development by Luis E
* [Steamlink:](http://steamcommunity.com/id/Bridge_Troll)